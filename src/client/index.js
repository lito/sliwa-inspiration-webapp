// @flow

import 'babel-polyfill';
import { AppContainer } from 'react-hot-loader';
import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import createStore from '../shared/redux/createStore';
import App from '../shared/App';
import { APP_CONTAINER_SELECTOR } from '../shared/config';

// eslint-disable-next-line no-underscore-dangle
const preloadedState = window.__PRELOADED_STATE__;
// eslint-disable-next-line no-underscore-dangle
delete window.__PRELOADED_STATE__;

const history = createHistory();
const store = createStore(history, preloadedState);

const rootEl = document.querySelector(APP_CONTAINER_SELECTOR);
const wrapApp = (AppComponent, reduxStore) => (
  <Provider store={reduxStore}>
    <ConnectedRouter history={history}>
      <AppContainer>
        <AppComponent />
      </AppContainer>
    </ConnectedRouter>
  </Provider>
);

if (!(rootEl instanceof Element)) {
  throw new Error('invalid type');
}
render(wrapApp(App, store), rootEl);
if (module.hot) {
  // flow-disable-next-line
  module.hot.accept('../shared/App', () => {
    // eslint-disable-next-line global-require
    const NextApp = require('../shared/App').default;
    render(wrapApp(NextApp, store), rootEl);
  });
}
