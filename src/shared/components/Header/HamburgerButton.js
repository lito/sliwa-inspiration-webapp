import * as React from 'react';
import { StyledHamburgerButton } from './Styled';

export default class HamburgerButton extends React.PureComponent<any> {
  render() {
    return (
      <StyledHamburgerButton style={this.props.style}>
        <button onClick={this.props.onClick}>
          <div className="icon-bar" />
          <div className="icon-bar" />
          <div className="icon-bar" />
        </button>
      </StyledHamburgerButton>
    );
  }
}
