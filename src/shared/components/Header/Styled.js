// @flow

import styled from 'styled-components';

export const StyledContactInfo = styled.div`
  padding: 1rem;
  flex-flow: row;
  width: 100%;
  .logo {
    width: 50px;
    margin-top: 1.5rem;
    margin-bottom: 1.5rem;
    margin-right: 20px;
    float: left;
  }
  .text {
    text-align: right;
    padding: 1.2rem 0.5rem 0;
    h2 {
      position: relative;
      font-weight: 200;
      font-size: 1.4rem;
      color: #fff;
    }
    p {
      margin-bottom: 0;
    }
    a {
      white-space: nowrap;
      color: hsla(0, 0%, 100%, 0.5);
    }
    span.fa {
      font-size: 0.8rem;
      margin-right: 0.2rem;
    }
  }
`;

export const StyledMenu = styled.div`
  margin: 1rem 0 0;
  font-family: 'Julius Sans One', sans-serif;
  font-size: 1.15rem;
  position: relative;
  transition: all 0.35s ease-in-out 0s;
  ul {
    width: 100%;
  }
  li.nav-item {
    width: 100%;
    a.nav-link {
      color: rgba(255, 255, 255, 0.5);
      outline: none;
      padding: 0.9rem 1.5rem;
      justify-content: space-between;
      display: flex;
      align-items: baseline;
      text-align: right;
      &:hover {
        background-color: transparent;
        span {
          color: #fff;
        }
      }
      span.label {
        font-family: 'Source Sans Pro', sans-serif;
        font-size: 1rem;
      }
      span.fa {
        float: left;
        font-size: 1rem;
        color: #41484c;
      }
      &.active {
        background: rgba(0, 0, 0, 0.15);
        box-shadow: inset 0 0 0.25em 0 rgba(0, 0, 0, 0.125);
        span.fa {
          color: #e27689;
        }
      }
    }
  }

  .navbar.navbar-toggleable {
    flex-flow: column wrap;
    padding: 0;
    .navbar-nav {
      flex-flow: column wrap;
    }
  }
`;

export const StyledHamburgerButton = styled.div`
  position: fixed;
  transition: all 0.5s ease;
  transition-property: margin-left;
  top: 0;
  z-index: 2;
  button {
    padding: 3px 6px;
    margin-top: 10px;
    margin-bottom: 5px;
    background-color: rgba(0, 0, 0, 0.2);
    border: 1px solid #fff;
    border-radius: 2px;
    
    &:focus{
      outline: none;
    }
  }

  .icon-bar {
    display: block;
    background-color: #fff;
    margin: 5px;
    width: 20px;
    height: 2px;
    border-radius: 1px;
  }
`;

export default styled.header`
  background: #222629;
  position: fixed;
  height: 100%;
  width: 250px;
  align-items: flex-start;
  color: hsla(0, 0%, 100%, 0.5);
  .sticky {
    position: sticky;
    width: 100%;
  }
`;
