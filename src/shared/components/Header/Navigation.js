// @flow

import * as React from 'react';
import {NavLink} from 'react-router-dom';
import {connect} from 'react-redux';
import {Navbar, Nav, NavItem} from 'reactstrap';
import Icon from 'react-fontawesome';
import {StyledMenu} from './Styled';
import {
  HOME_PAGE_ROUTE,
  PRODUCT_PAGE_ROUTE,
  CONTACT_PAGE_ROUTE,
  SERVICES_PAGE_ROUTE,
  PATENTS_PAGE_ROUTE,
  PORTFOLIO_PAGE_ROUTE
} from '../../routes';

const navigationLinks = [
  {route: HOME_PAGE_ROUTE, label: 'Intro', icon: 'fa-home', name: 'start'},
  {
    route: PORTFOLIO_PAGE_ROUTE,
    label: 'Portfolio',
    icon: 'fa-th',
    name: 'portfolio'
  },
  {
    route: PRODUCT_PAGE_ROUTE,
    label: 'Produkte',
    icon: 'fa-gears',
    name: 'product'
  },
  {
    route: SERVICES_PAGE_ROUTE,
    label: 'Leistungen',
    icon: 'fa-wrench',
    name: 'service'
  },
  {
    route: PATENTS_PAGE_ROUTE,
    label: 'Patente',
    icon: 'fa-certificate',
    name: 'patent'
  },
  {
    route: CONTACT_PAGE_ROUTE,
    label: 'Kontakt',
    icon: 'fa-envelope',
    name: 'contact'
  }
];

type Props = {
  activeMenu?: string,
  onNavItemClick?: func,
};

class Navigation extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    this.renderNavLink = this.renderNavLink.bind(this);
  }

  renderNavLink(link) {
    const {onNavItemClick} = this.props;
    return (
      <NavItem key={link.route}>
        <NavLink
          className="nav-link"
          to={link.route}
          activeStyle={{color: '#fff'}}
          onClick={onNavItemClick}
          isActive={(match, location) => {
            if (this.props.activeMenu) {
              return link.name === this.props.activeMenu;
            }
            return location.pathname === link.route;
          }}
        >
          <Icon className={link.icon}/> {' '}
          <span className="label">{link.label}</span>
        </NavLink>
      </NavItem>
    );
  }

  render() {
    return (
      <StyledMenu>
        <Navbar toggleable>
          <Nav navbar>
            {navigationLinks && navigationLinks.map(this.renderNavLink)}
          </Nav>
        </Navbar>
      </StyledMenu>
    );
  }
}

export default connect(state => {
  const pathname = state.router.location && state.router.location.pathname;
  return {
    pathname
  };
})(Navigation);
