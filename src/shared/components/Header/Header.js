// @flow

import React, { PureComponent } from 'react';
import Icon from 'react-fontawesome';
import StyledHeader, { StyledContactInfo } from './Styled';
import Navigation from './Navigation';

type Props = {
  logoUrl: string,
  author: string,
  phone: string,
  mobile: string,
  email: string,
  style: string,
  onNavItemClick: func,
};

class Header extends PureComponent {
  constructor(props: Props) {
    super(props);
    this.state = {
      logoUrl: 'http://sliwa-inspiration.com/static/assets/images/avatar.jpg',
      author: 'Leszek Sliwa',
      phone: '+49-89-750 792 64',
      mobile: '+49 173 51 73 69 1',
      email: 'info@sliwa-inspiration.com'
    };
  }

  renderContactTeaser() {
    const { logoUrl, author, phone, mobile, email } = this.state;
    return (
      <StyledContactInfo>
        <div className="logo">
          <a href="/">
            <img className="img-fluid" alt={logoUrl} src={logoUrl} />
          </a>
        </div>
        <div className="text">
          <h2>{author}</h2>
          <p>
            <Icon className="fa-phone" /> {phone}
          </p>
          <p>
            <Icon className="fa-mobile" /> {mobile}
          </p>
          <p>
            <a href={`mailto: ${email}`}>
              <Icon className="fa-envelope" /> {email}
            </a>
          </p>
        </div>
      </StyledContactInfo>
    );
  }

  render() {
    const { style, onNavItemClick } = this.props;
    return (
      <StyledHeader className="header" style={style}>
        <div className="sticky">
          {this.renderContactTeaser()}
          <Navigation {...this.props} onNavItemClick={onNavItemClick}/>
        </div>
      </StyledHeader>
    );
  }
}

export default Header;
