// @flow

import * as React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const StyledTeaser = styled.div`
  img {
    height: 300px;
  }
  div.teaser {
    margin: 2rem 0 0;
    padding: 0 1rem 0 0;
    a {
      color: #000;
      font-size: 1rem;
      font-family: 'Julius Sans One', sans-serif;
    }
  }
`;

type Props = {
  content: {
    headline: string,
    image: {
      url: string
    },
    link: {
      entity: {
        target: string
      }
    }
  }
};

const Teaser = (props: Props) => {
  const attributes = {
    className: 'teaser'
  };
  return (
    <StyledTeaser className="d-flex stretch-height">
      <div {...attributes}>
        <Link to={props.content.link.entity.target}>
          <img src={props.content.image.url} alt="" />
          <h4>{props.content.headline}</h4>
        </Link>
      </div>
    </StyledTeaser>
  );
};

export default Teaser;
