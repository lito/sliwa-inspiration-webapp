// @flow

import * as React from 'react';
// eslint-disable-next-line
import {normalizeFileName} from 'visar-shared';
import Image from './Image';
import Video from './Video/Video';

type Props = {
  content: {
    files: Array<any>
  }
};

export default class Media extends React.PureComponent<Props, any> {
  static renderItem(item: any) {
    return item.entity.type === 'module_video' ? (
      <article className="item">
        <Video
          url={item.entity.video.entity.url}
          poster={item.entity.poster.url}
          key={item.entity.video.url}
        />
      </article>
    ) : (
      <article className="item">
        <Image
          index={item.entity.image.url}
          src={item.entity.image.url}
          key={item.entity.image.url}
        />
      </article>
    );
  }

  render() {
    const files = this.props.content.files;

    return (
      <div className="container">
        <div className="row">
          <div className="col-xs-12 col-md-4">
            {files.map(
              (item, index) => (index % 3 === 0 ? Media.renderItem(item) : null)
            )}
          </div>
          <div className="col-xs-12 col-md-4">
            {files.map(
              (item, index) => (index % 3 === 1 ? Media.renderItem(item) : null)
            )}
          </div>
          <div className="col-xs-12 col-md-4">
            {files.map(
              (item, index) => (index % 3 === 2 ? Media.renderItem(item) : null)
            )}
          </div>
        </div>
      </div>
    );
  }
}
