// @flow

import React from 'react';
import { connect } from 'react-redux';
import { getCurrentRoute } from '../utils/content';
import { scrollToSection } from '../utils/frontend';

let onScroll;

function isInViewport(element) {
  const elementTop = element.getBoundingClientRect().top;
  const elementBottom = element.getBoundingClientRect().bottom;
  return (
    (elementTop >= (window.innerHeight / 2) * -1 &&
      elementTop <= window.innerHeight / 2) ||
    (elementBottom > 0 && elementBottom - (window.innerHeight / 2) > 0)
  );
}

function withScrollToSection(WrappedComponent: any) {
  class ScrollToSection extends React.Component {
    constructor(props: {}) {
      super(props);
      this.state = {
        isScrolling: false,
        hasTopPosition: true,
        activeMenu: null
      };
      this.onScroll = this.onScroll.bind(this);
      this.checkActiveElement = this.checkActiveElement.bind(this);
    }
    componentDidMount() {
      this.onScroll();
      this.scrollToSection();
    }
    componentDidUpdate(prevProps: any) {
      if (
        (this.props.isReady && !prevProps.isReady) ||
        this.props.currentRouteName !== prevProps.currentRouteName
      ) {
        this.resetScrollState();
        this.scrollToSection();
      }
    }
    onScroll() {
      let scrollTimer;
      if (onScroll) {
        clearTimeout(onScroll);
      }
      // eslint-disable-next-line
      onScroll = window.onscroll = () => {
        if (scrollTimer !== null) {
          clearTimeout(scrollTimer);
        }
        if (!this.state.isScrolling) {
          this.setState({
            isScrolling: true
          });
        }
        scrollTimer = setTimeout(() => {
          this.checkActiveElement();
          this.setState({
            isScrolling: false,
            hasTopPosition: !window.pageYOffset
          });
        }, 500);
      };
    }
    resetScrollState() {
      if (onScroll) {
        clearTimeout(onScroll);
      }
      this.setState({
        activeMenu: null
      });
    }
    checkActiveElement() {
      const sections = document.querySelectorAll('[data-section]');
      let foundActive;
      sections.forEach(section => {
        if (foundActive) return;
        const name = section.dataset && section.dataset.section;
        if (isInViewport(section)) {
          if (this.state.activeMenu !== name) {
            this.setState({
              activeMenu: name
            });
          }
          foundActive = true;
        }
      });
      if (!foundActive && !this.state.activeMenu) {
        this.setState({
          activeMenu: null
        });
      }
    }
    scrollToSection() {
      if (this.props.currentRouteName) {
        scrollToSection(this.props.currentRouteName);
      }
    }
    props: any;
    render() {
      return <WrappedComponent {...this.state} {...this.props} />;
    }
  }

  return connect(state => {
    let currentRouteName;
    let isReady;
    const pageId = 'page_dynamic-home';
    const content = state.content.data ? state.content.data[pageId] : null;
    if (state.router) {
      const route = getCurrentRoute(state.router.location.pathname);
      currentRouteName = route.name || route.path.substr(1);
      isReady = content && route.withScrolling;
    }
    return {
      currentRouteName,
      isReady
    };
  })(ScrollToSection);
}

export default withScrollToSection;
