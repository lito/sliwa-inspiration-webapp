// @flow

import * as React from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'react-redux';
// eslint-disable-next-line
import { getFetcher } from 'visar-shared';
import { getCurrentRoute } from '../utils/content';

type Props = {
  content: any,
  location: {
    pathname: string
  },
  dispatch: Dispatch
};

function withFetching(WrappedComponent: any) {
  class FetchedComponent extends React.Component<Props> {
    componentDidMount() {
      this.fetchData();
    }
    componentDidUpdate(prevProps: any) {
      if (this.props.location !== prevProps.location) {
        this.fetchData();
      }
    }
    fetchData() {
      if (!this.props.content && this.props.location) {
        const { pathname } = this.props.location;
        const route = getCurrentRoute(pathname);
        if (route) {
          this.props.dispatch(getFetcher(route));
        }
      }
    }
    render() {
      return <WrappedComponent {...this.props} />;
    }
  }
  return connect()(FetchedComponent);
}

export default withFetching;
