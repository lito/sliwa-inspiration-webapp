// @flow

import styled from 'styled-components';

// eslint-disable-next-line
export const VideoWrapper = styled.div`
  width: auto;

  video {
    width: 100%;
  }
`;
