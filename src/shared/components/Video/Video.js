// @flow

import * as React from 'react';

import { VideoWrapper } from './Styled';

export default class Video extends React.PureComponent<{
  url: string,
  poster: string
}> {
  render() {
    return (
      <VideoWrapper>
        <video
          controls
          playsInline
          preload="none"
          poster={this.props.poster}
        >
          <source src={this.props.url} type="video/mp4" />
        </video>
      </VideoWrapper>
    );
  }
}
