// @flow

import * as React from 'react';
import Lightbox from 'react-image-lightbox';
// eslint-disable-next-line
import { normalizeFileName } from 'visar-shared';
import Thumbnail from './Thumbnail';

type Props = {
  className: string,
  content: {
    images: Array<any>
  },
  fullWidth: boolean
};

export default class Gallery extends React.Component<
  Props,
  {
    isOpen: boolean,
    index: number
  }
> {
  static getPrevSrc(index: number, images: Array<any>) {
    const prevImage = images[index - 1];
    if (!prevImage) {
      return null;
    }
    return prevImage.url;
  }

  static getNextSrc(index: number, images: Array<any>) {
    const nextImage = images[index + 1];
    if (!nextImage) {
      return null;
    }
    return nextImage.url;
  }

  constructor(props: Props) {
    super(props);
    this.onThumbnailClick = this.onThumbnailClick.bind(this);
    this.state = {
      isOpen: false,
      index: 0
    };
  }
  onThumbnailClick: Function;
  onThumbnailClick(index: number) {
    this.setState({
      isOpen: true,
      index
    });
  }
  getClassNames: Function;
  getClassNames() {
    const classNames = ['gallery'];
    if (this.props.className) {
      classNames.push(this.props.className);
    }
    if (!this.props.fullWidth) {
      classNames.push('container');
    }
    return classNames.join(' ');
  }
  render() {
    const { isOpen, index } = this.state;
    const images = this.props.content.images;
    const thumbnails =
      images &&
      images.map((item, imageIndex) => {
        const url = item.url;
        let name;
        if (item.entity) {
          const { filename } = item.entity;
          name = filename.substr(0, filename.lastIndexOf('.'));
        }
        return (
          <Thumbnail
            onClick={this.onThumbnailClick}
            name={name}
            url={url}
            key={url}
            index={imageIndex}
            className={'col-md-4 col-xs-12'}
          />
        );
      });
    return (
      <div className={this.getClassNames()}>
        <div className="row">
          {thumbnails}
          {isOpen && (
            <Lightbox
              mainSrc={images[index].url}
              prevSrc={Gallery.getPrevSrc(index, images)}
              nextSrc={Gallery.getNextSrc(index, images)}
              onCloseRequest={() => this.setState({ isOpen: false })}
              onMovePrevRequest={() =>
                this.setState({
                  index: index - 1
                })
              }
              onMoveNextRequest={() =>
                this.setState({
                  index: index + 1
                })
              }
              imageTitle={
                images[index].entity &&
                normalizeFileName(images[index].entity.filename)
              }
              enableZoom={false}
            />
          )}
        </div>
      </div>
    );
  }
}
