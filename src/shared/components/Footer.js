// @flow

import * as React from 'react';
import { NavLink } from 'react-router-dom';
import { Navbar, Nav, NavItem } from 'reactstrap';
import styled from 'styled-components';
import { PRIVACY_PAGE_ROUTE, IMPRINT_PAGE_ROUTE } from '../routes';

const navigationLinks = [
  { route: IMPRINT_PAGE_ROUTE, label: 'Impressum' },
  { route: PRIVACY_PAGE_ROUTE, label: 'Datenschutz' }
];

const FooterContent = styled.div`
  box-shadow: inset 0 1px 0 0 rgba(0,0,0,.05), inset 0 0.1em 0.1em 0 rgba(0,0,0,.025);
  color: #737373;
  background-color: #dce3e2;
  font-size: 1.2rem;
  padding: 2rem 0 1rem;
`;

const CopyrightText = styled.p`
  text-align: center;
  padding: 0;
  margin: 0;
`;

const Footer = () => (
  <footer>
    <FooterContent>
      <CopyrightText>© 2018 SLIWA Inspiration. Munchen - Deutschland</CopyrightText>
      <Navbar light toggleable >
        <Nav className="mx-auto" navbar>
          {navigationLinks &&
            navigationLinks.map(link => (
              <NavItem key={link.route}>
                <NavLink
                  className="nav-link"
                  to={link.route}
                  activeStyle={{ color: '#000' }}
                  exact
                >
                  {link.label}
                </NavLink>
              </NavItem>
            ))}
        </Nav>
      </Navbar>
    </FooterContent>
  </footer>
);

export default Footer;
