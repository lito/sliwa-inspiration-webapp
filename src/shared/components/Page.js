// @flow

import * as React from 'react';
import Helmet from 'react-helmet';
import { APP_NAME } from '../config';

const Page = (props: { title: string }) => (
  <div>
    <Helmet
      meta={[
        { name: 'description', content: 'Hello from Visarsoft' },
        { property: 'og:title', content: APP_NAME }
      ]}
    />
    <p>{props.title}</p>
  </div>
);

export default Page;
