// @flow

import * as React from 'react';

export default class Image extends React.PureComponent<{
  src: string
}> {
  render() {
    return (
      <img
        src={this.props.src}
        alt=" "
      />
    );
  }
}
