// @flow

import React from 'react';
import Helmet from 'react-helmet';
import StyledTextPage, { Title as StyledTitle } from './Styled';
import { APP_NAME } from '../../config';
import withFetching from './../withFetching';

const TextPage = (props: { content: any }) => {
  if (props.content) {
    return (
      <StyledTextPage className="container" {...props}>
        <Helmet
          titleTemplate={`%s | ${APP_NAME}`}
          title={props.content.headline}
          meta={[
            { name: 'description', content: 'Hello from Visarsoft' },
            { property: 'og:title', content: props.content.headline }
          ]}
        />
        <StyledTitle>{props.content.headline}</StyledTitle>
        <div className="container">
          <div className="col">
            <div
              dangerouslySetInnerHTML={{ __html: props.content.body.value }}
            />
          </div>
        </div>
      </StyledTextPage>
    );
  }
  return null;
};

export default withFetching(TextPage);
