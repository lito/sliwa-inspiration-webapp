// @flow

import styled from 'styled-components';

export const Title = styled.h1`
  padding-top: 20px;
  padding-bottom: 20px;
  font-size: 2rem;
  text-align: center;
  font-family: 'Julius Sans One', sans-serif;
  color: #292b2c;
`;

export default styled.div`
  padding-top: 20px;
  color: #737373;
  font-size: 1rem;
  text-align: justify;
  text-align-last: center;
  -moz-text-align-last: center;

  h1,
  h2,
  h3 {
    text-align: center;
  }
`;
