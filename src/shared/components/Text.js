// @flow

import * as React from 'react';
// eslint-disable-next-line
import { parseBody } from 'visar-shared';

type Props = {
  content?: {
    body: {
      value: string
    }
  },
};

const Text = ({ content }: Props) =>
content && (
  <div
    className="text"
    dangerouslySetInnerHTML={{ __html: parseBody(content.body.value) }}
  />
);

export default Text;
