// @flow

import styled from 'styled-components';

export default styled.div`
  .section {
    box-shadow: inset 0 1px 0 0 rgba(0, 0, 0, 0.05),
      inset 0 0.1em 0.1em 0 rgba(0, 0, 0, 0.025);
  }
  .portfolio {
    background-color: #f5fafa;
    .item {
      padding-bottom: 30px;
    }
  }
  .product {
    background-color: #ecf1f1;
  }
  .service, .patent, .contact {
    background-color: #e8edec;
  }
  div.headline {
    background: none;
  }
  h1,
  .headline h2 {
    font-size: 3rem;
    font-family: 'Source Sans Pro', sans-serif;
    color: #666;
  }
  .text {
    max-width: 1200px;
    margin: 0 auto;
    text-align: center;
    padding: 2rem 2rem 4rem;
    color: #666;
    line-height: 2.5rem;
    strong {
      font-weight: bold;
      font-size: 2rem;
    }
  }
  .teaser-list .row {
    justify-content: center;
  }
  .form-wrapper {
    background: none;
    padding-bottom: 2rem;
    .form-area {
      width: 100%;
      padding-right: 15px;
      padding-left: 15px;
      margin-right: auto;
      margin-left: auto;
    }
    @media (min-width: 375px) {
      .container {
        width: 365px;
      }
    }
    @media (min-width: 576px) {
      .container {
        width: 562px;
      }
    }
    @media (min-width: 768px) {
      .container {
        width: 750px;
      }
    }
    @media (min-width: 992px) {
      .container {
        width: 970px;
      }
    }
    @media (min-width: 1200px) {
      .container {
        width: 1170px;
      }
    }
  }
  img {
    max-width: 100%;
  }
  form input,
  form textarea {
    background-color: #fff;
    font-family: 'Muli', sans-serif;
    font-size: 0.9rem;
    padding: 12px;
  }
  form button {
    background-color: #ddd;
    border: 2px solid #919394;
    box-shadow: none !important;
    border-radius: 0;
    color: #666;
    height: 77px;
    width: 25%;
    margin: 16px auto;
    font-family: 'Muli', sans-serif;
    outline: none;
    &:active {
      background-color: #e27689;
      border: 2px solid #919394;
    }
    &:hover {
      background-color: #e27689;
      border: 2px solid #919394;
    }
    &:focus {
      outline-color: transparent;
      outline-style: none;
    }
    @media (max-width: 700px) {
      width: 100%;
    }
  }
`;
