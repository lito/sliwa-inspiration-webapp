// @flow

import * as React from 'react';
// eslint-disable-next-line
import { DynamicPage } from 'visar-shared';
import withFetching from '../withFetching';
import Teaser from '../Teaser';
import Gallery from '../Gallery';
import Media from '../Media';
import Text from '../Text';
import StyledDynamicPage from './Styled';

const Dynamic = props => (
  <StyledDynamicPage>
    <DynamicPage
      components={{
        module_teaser_list: {
          components: {
            teaser: Teaser
          }
        },
        module_form: {
          props: Object.assign({
            className: 'form-area'
          }, props)
        },
        module_media: {
          self: Media
        },
        module_gallery: {
          self: Gallery
        },
        module_text: {
          self: Text
        }
      }}
      {...props}
    />
  </StyledDynamicPage>
);

export default withFetching(Dynamic);
