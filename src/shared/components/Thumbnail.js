// @flow

import * as React from 'react';
// eslint-disable-next-line
import { resolveThumbnailPath } from 'visar-shared';

export default class Thumbnail extends React.Component<{
  index?: number,
  url: string,
  name?: string,
  className?: string,
  onClick?: Function
}> {
  onClick(event: Event) {
    event.preventDefault();
    if (this.props.onClick) {
      this.props.onClick(this.props.index);
    }
  }
  getClassNames() {
    const classNames = [];
    if (this.props.className) {
      classNames.push(this.props.className);
    }
    return classNames.join(' ');
  }
  render() {
    const { name, url } = this.props;
    return (
      <div className={this.getClassNames()}>
        <div
          onClick={(event: Event) => this.onClick(event)}
          role="button"
          tabIndex={this.props.index}
        >
          <img
            alt={name}
            src={resolveThumbnailPath(url)}
            style={{
              width: '100%',
              paddingBottom: '30px'
            }}
          />
        </div>
      </div>
    );
  }
}
