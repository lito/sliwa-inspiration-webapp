// @flow

export const WEB_PORT = process.env.PORT || 8000;
export const STATIC_PATH = '/static';
export const APP_NAME = 'Sliwa';
export const WDS_PORT = 7000;
export const APP_CONTAINER_CLASS = 'js-app';
export const APP_CONTAINER_SELECTOR = `.${APP_CONTAINER_CLASS}`;
export const APP_BASE_URL: string = 'http://sliwa.visarsoft.de';
export const API_BASE_URL: string = 'http://content.sliwa-inspiration.com';
export const MESSAGE_BASE_URL: string = 'http://message-service.visarsoft.de';
export const MESSAGE_CONSUMER: string = 'visarsoft';
export const MESSAGE_RECIPIENT: string = 'web@visarsoft.de';
