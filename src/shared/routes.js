// @flow

import HomePage from './containers/homePage';
import TextPage from './containers/textPage';
import { loadPageData } from './redux/content/actions';

export const HOME_PAGE_ROUTE = '/';
export const PRODUCT_PAGE_ROUTE = '/product';
export const SERVICES_PAGE_ROUTE = '/service';
export const PATENTS_PAGE_ROUTE = '/patent';
export const PORTFOLIO_PAGE_ROUTE = '/portfolio';
export const CONTACT_PAGE_ROUTE = '/contact';
export const PRIVACY_PAGE_ROUTE = '/privacy';
export const IMPRINT_PAGE_ROUTE = '/imprint';
export const NOT_FOUND_DEMO_PAGE_ROUTE = '/404';

export default [
  {
    exact: true,
    path: HOME_PAGE_ROUTE,
    key: HOME_PAGE_ROUTE,
    name: 'start',
    component: HomePage,
    withScrolling: true,
    promise: () =>
      loadPageData({
        type: 'page_dynamic',
        title: 'home'
      })
  },
  {
    exact: true,
    path: PRODUCT_PAGE_ROUTE,
    component: HomePage,
    key: PRODUCT_PAGE_ROUTE,
    withScrolling: true,
    promise: () =>
      loadPageData({
        type: 'page_dynamic',
        title: 'home'
      })
  },
  {
    exact: true,
    path: PORTFOLIO_PAGE_ROUTE,
    component: HomePage,
    key: PORTFOLIO_PAGE_ROUTE,
    withScrolling: true,
    promise: () =>
      loadPageData({
        type: 'page_dynamic',
        title: 'home'
      })
  },
  {
    exact: true,
    path: SERVICES_PAGE_ROUTE,
    component: HomePage,
    key: SERVICES_PAGE_ROUTE,
    withScrolling: true,
    promise: () =>
      loadPageData({
        type: 'page_dynamic',
        title: 'home'
      })
  },
  {
    exact: true,
    path: PATENTS_PAGE_ROUTE,
    component: HomePage,
    key: PATENTS_PAGE_ROUTE,
    withScrolling: true,
    promise: () =>
      loadPageData({
        type: 'page_dynamic',
        title: 'home'
      })
  },
  {
    exact: true,
    path: CONTACT_PAGE_ROUTE,
    component: HomePage,
    key: CONTACT_PAGE_ROUTE,
    withScrolling: true,
    promise: () =>
      loadPageData({
        type: 'page_dynamic',
        title: 'home'
      })
  },
  {
    path: PRIVACY_PAGE_ROUTE,
    component: TextPage,
    key: PRIVACY_PAGE_ROUTE,
    promise: () =>
      loadPageData({
        type: 'page_text',
        title: 'privacy'
      })
  },
  {
    path: IMPRINT_PAGE_ROUTE,
    component: TextPage,
    key: IMPRINT_PAGE_ROUTE,
    promise: () =>
      loadPageData({
        type: 'page_text',
        title: 'imprint'
      })
  },
  {
    key: NOT_FOUND_DEMO_PAGE_ROUTE,
    component: HomePage,
    promise: () =>
      loadPageData({
        type: 'page_dynamic',
        title: 'home'
      })
  }
];
