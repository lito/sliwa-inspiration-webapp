// @flow

import * as configs from '../config';

export const isProd: boolean = process.env.NODE_ENV === 'production';
export const getEnv: Function = (): string => {
  if (process.env.NODE_ENV) {
    return process.env.NODE_ENV;
  }
  return '';
};

export function getConfig() {
  return configs;
}

export function getAppBaseUrl() {
  const config = getConfig();
  return isProd ? config.APP_BASE_URL : `http://localhost:${config.WEB_PORT}`;
}
