// @flow

import smoothscroll from 'smoothscroll-polyfill';

// eslint-disable-next-line
export function scrollToSection(name: string) {
  smoothscroll.polyfill();
  let positionY = 0;
  const sectionNode = document.querySelector(`[data-section=${name}]`);
  if (sectionNode) {
    positionY = sectionNode.offsetTop;
    window.scroll({
      top: positionY,
      left: 0,
      behavior: 'smooth'
    });
  }
}
