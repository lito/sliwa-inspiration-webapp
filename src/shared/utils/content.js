// @flow

import { matchPath } from 'react-router-dom';
import routes from '../../shared/routes';

// eslint-disable-next-line
export const getCurrentRoute = (pathname: string): any => {
  let currentRoute;
  routes.some(route => {
    const match = matchPath(pathname, route);
    if (match && match.isExact) {
      currentRoute = route;
    }
    return match;
  });
  return currentRoute;
};
