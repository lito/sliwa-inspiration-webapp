// @flow

import React from 'react';
import { connect } from 'react-redux';
import Text from '../components/TextPage/TextPage';

const getTitleParam = match => match.url.replace(/\//, '');

const TextPage = (props: { content: any }) => (
  <Text content={props.content} {...props} />
);

const mapStateToProps = (state, props) => {
  const content =
    (state.content.data &&
      state.content.data[`page_text-${getTitleParam(props.match)}`]) ||
    null;
  return { content };
};

export default connect(mapStateToProps)(TextPage);
