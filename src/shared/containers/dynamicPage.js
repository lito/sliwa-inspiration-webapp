// @flow

import { connect } from 'react-redux';
import Dynamic from '../components/DynamicPage/Dynamic';

const mapStateToProps = state => {
  let pageId;
  if (state.router && state.router.location) {
    const name = state.router.location.pathname.replace(/^\//, '');
    pageId = `page_dynamic-${name}`;
  }

  const content = state.content.data ? state.content.data[pageId] : null;
  return { content };
};

export default connect(mapStateToProps)(Dynamic);
