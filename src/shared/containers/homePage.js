// @flow

import { connect } from 'react-redux';
import { doMsRequest as onSubmit } from '../redux/user/actions';
import Dynamic from '../components/DynamicPage/Dynamic';

const mapStateToProps = state => {
  const pageId = 'page_dynamic-home';
  const content = state.content.data ? state.content.data[pageId] : null;
  return {
    content,
    error: state.user.rejected,
    loading: state.user.pending
  };
};

export default connect(mapStateToProps, { onSubmit })(Dynamic);
