// @flow

import styled from 'styled-components';

export default styled.div`
  @import url(https://fonts.googleapis.com/css?family=Source+Sans+Pro:300);
  background: #ebebeb;
  font-family: 'Source Sans Pro', sans-serif;
  position: relative;
  .main {
    float: right;
    overflow-x: hidden;
  }
`;
