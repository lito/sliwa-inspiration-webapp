// @flow

import { merge, cloneDeep } from 'lodash';
import {
  SUBMIT_FORM_PENDING,
  SUBMIT_FORM_COMPLETED,
  SUBMIT_FORM_REJECTED
} from './actions';

type State = {
  pending: boolean,
  completed: boolean,
  rejected: any,
  loggedIn: boolean
};

const initialState = {
  pending: false,
  completed: false,
  rejected: null,
  loggedIn: false,
  customerId: null
};

export default function reducer(state: State = initialState, action: Object) {
  const newState = cloneDeep(state);
  switch (action.type) {
    case SUBMIT_FORM_PENDING: {
      return merge({
        pending: true,
        completed: false,
        rejected: null
      });
    }
    case SUBMIT_FORM_COMPLETED: {
      return merge({
        completed: true,
        pending: false
      });
    }
    case SUBMIT_FORM_REJECTED: {
      return merge({
        rejected: action.payload,
        pending: false
      });
    }
    default:
      return newState;
  }
}
