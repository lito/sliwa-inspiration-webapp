// @flow

import axios from 'axios';

export const SUBMIT_FORM_PENDING = 'user/SUBMIT_FORM_PENDING';
export const SUBMIT_FORM_COMPLETED = 'user/SUBMIT_FORM_COMPLETED';
export const SUBMIT_FORM_REJECTED = 'user/SUBMIT_FORM_REJECTED';

export function submitStart() {
  return { type: SUBMIT_FORM_PENDING };
}

export function submitSuccess(payload: Object) {
  return { type: SUBMIT_FORM_COMPLETED, payload };
}

export function submitFailure(payload: Object) {
  return { type: SUBMIT_FORM_REJECTED, payload };
}

export function doMsRequest({
  name,
  sender,
  telephone,
  message
}: {
  name: string,
  sender: string,
  telephone: string,
  message: string
}) {
  // flow-disable-next-line
  return dispatch => {
    dispatch(submitStart());
    return axios
      .post('/api/message', {
        sender,
        subject: `Contact request from Name: ${name} with Tel: ${telephone}`,
        body: message
      })
      .then(res => {
        const data = res.data;
        dispatch(submitSuccess(data));
      })
      .catch(err => {
        if (err.response) {
          dispatch(submitFailure(err.response.data));
        }
      });
  };
}
