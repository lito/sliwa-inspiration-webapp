// @flow

import { merge, cloneDeep } from 'lodash';
import { DATA_PENDING, DATA_FULFILLED, DATA_REJECTED } from './actions';

type State = {
  pending: boolean,
  fulfilled: boolean,
  rejected: any
};

const initialState = {
  pending: false,
  fulfilled: false,
  rejected: null,
  data: {}
};

export default function reducer(state: State = initialState, action: Object) {
  const newState = cloneDeep(state);
  switch (action.type) {
    case DATA_PENDING: {
      return merge(newState, {
        pending: true,
        fulfilled: false
      });
    }
    case DATA_FULFILLED: {
      return merge(newState, {
        fulfilled: true,
        pending: false,
        data: action.payload
      });
    }
    case DATA_REJECTED: {
      return merge(newState, {
        rejected: 'error'
      });
    }
    default:
      return newState;
  }
}
