// @flow

import axios from 'axios';
import { getAppBaseUrl } from '../../utils/env';

export const DATA_PENDING = 'content/DATA_PENDING';
export const DATA_FULFILLED = 'content/DATA_FULFILLED';
export const DATA_REJECTED = 'content/DATA_REJECTED';

export function startDataLoading() {
  return { type: DATA_PENDING };
}

export function loadDataSuccess(d: Object, pageId: String) {
  return { type: DATA_FULFILLED, page: pageId, data: d };
}

export function loadPageDataSuccess(payload: Object) {
  return { type: DATA_FULFILLED, payload };
}

export function loadDataFailure(payload: Object) {
  return { type: DATA_REJECTED, payload };
}

export function loadPageData(params: { type: string, title?: string }): any {
  // flow-disable-next-line
  return (dispatch, getState) => {
    const state = getState();
    const pageId = `${params.type}${params.title ? `-${params.title}` : ''}`;
    if (state.content.data[pageId]) {
      return Promise.resolve();
    }
    dispatch(startDataLoading());
    return axios
      .get(`${getAppBaseUrl()}/api/content`, {
        params
      })
      .then(res => {
        const pageContent = res.data.data.page;
        dispatch(
          loadPageDataSuccess({
            [pageId]:
              pageContent && pageContent.entities && pageContent.entities[0]
          })
        );
        return res;
      })
      .catch(err => {
        if (err.response) {
          dispatch(loadDataFailure(err));
        }
      });
  };
}

export function loadCustomerPageData(params: { type: string }): Promise<void> {
  // flow-disable-next-line
  return (dispatch, getState) => {
    const state = getState();
    return new Promise((resolve, reject) => {
      const { customerId } = state.user;
      if (!customerId) {
        return reject(new Error('User is not authorised'));
      }
      return loadPageData({
        type: params.type,
        title: state.user.customerId
      })(dispatch, getState)
        .then(res => resolve(res))
        .catch(err => {
          if (err.response) {
            reject(err);
          }
        });
    });
  };
}

export function loadDynamicPageByPath(): Promise<void> {
  // flow-disable-next-line
  return (dispatch, getState) => {
    const state = getState();
    return new Promise((resolve, reject) => {
      let title;
      if (state.router && state.router.location) {
        title = state.router.location.pathname.replace(/^\//, '');
      }
      if (!title) {
        return reject(new Error('Path is not defined'));
      }
      return loadPageData({
        type: 'page_dynamic',
        title
      })(dispatch, getState)
        .then(res => resolve(res))
        .catch(err => {
          if (err.response) {
            reject(err);
          }
        });
    });
  };
}
