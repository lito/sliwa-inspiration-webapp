// @flow

import _ from 'lodash';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import thunkMiddleware from 'redux-thunk';
import * as reducers from './';

function getInitialState(initialState: any = {}, cookies: any = {}) {
  const state = initialState;
  const { customerId, loggedIn } = cookies;
  if (customerId && loggedIn) {
    state.user = {
      customerId,
      loggedIn
    };
  }
  return state;
}

export default (
  initialHistory: any = null,
  initialState: any = {},
  cookies: any = {}
) => {
  const middleware = [thunkMiddleware];
  let rootReducer = reducers;
  if (initialHistory) {
    rootReducer = _.merge(reducers, { router: routerReducer });
    middleware.push(routerMiddleware(initialHistory));
  }
  const enhancers = [];
  /* eslint-disable no-underscore-dangle */
  const composeEnhancers =
    (typeof window !== 'undefined' &&
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
    compose;
  const store = createStore(
    combineReducers(rootReducer),
    getInitialState(initialState, cookies),
    composeEnhancers(applyMiddleware(...middleware), ...enhancers)
  );

  return store;
};
