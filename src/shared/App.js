// @flow

import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import Helmet from 'react-helmet';
import withScrollToSection from './components/withScrollToSection';
import Header from './components/Header/Header';
import Footer from './components/Footer';
import routes from './routes';
import { APP_NAME } from './config';
import StyledApp from './StyledApp';
import HamburgerButton from './components/Header/HamburgerButton';

class App extends React.PureComponent<any> {
  constructor() {
    super();
    this.state = {
      isSidebarShown: false,
      isHamburgerBtnShown: false
    };
    this.toggleSidebar = this.toggleSidebar.bind(this);
    this.onNavItemClick = this.onNavItemClick.bind(this);
  }

  componentDidMount() {
    this.setSidebarVisibility();
    window.addEventListener('resize', () => {
      this.setSidebarVisibility();
    });
  }

  componentWillUnmount() {
    window.removeEventListener('resize', false);
  }

  onNavItemClick() {
    if (window.innerWidth < 768) {
      this.setState({
        isSidebarShown: false
      });
    }
  }

  setSidebarVisibility() {
    if (window.innerWidth < 768) {
      this.setState({
        isHamburgerBtnShown: true,
        isSidebarShown: false,
      });
    } else {
      this.setState({
        isHamburgerBtnShown: false,
        isSidebarShown: true
      });
    }
  }

  toggleSidebar() {
    this.setState({
      isSidebarShown: !this.state.isSidebarShown
    });
  }

  render() {
    return (
      <StyledApp>
        <Helmet titleTemplate={`%s | ${APP_NAME}`} defaultTitle={APP_NAME} />
        <Header
          {...this.props}
          onNavItemClick={this.onNavItemClick}
          style={{ display: this.state.isSidebarShown ? 'block' : 'none' }}
        />
        {this.state.isHamburgerBtnShown
          ? <HamburgerButton
            onClick={this.toggleSidebar}
            style={{ marginLeft: this.state.isSidebarShown ? '260px' : '10px' }}
          />
          : null
        }
        <div
          className="main"
          style={{
            width: this.state.isSidebarShown ? 'calc(100% - 250px)' : '100%'
          }}
        >
          <Switch>{routes.map(route => <Route {...route} />)}</Switch>
          <Footer />
        </div>
      </StyledApp>
    );
  }
}

export default withScrollToSection(App);
