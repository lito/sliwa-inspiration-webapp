// @flow

import { matchPath } from 'react-router-dom';
import createMemoryHistory from 'history/createMemoryHistory';
// eslint-disable-next-line
import { getFetcher } from 'visar-shared';
import renderHtml from './renderHtml';
import routes from '../shared/routes';
import createStore from '../shared/redux/createStore';

export default (app: Object) => {
  app.get('*', (req, res, next) => {
    const { originalUrl } = req;
    const history = createMemoryHistory({
      initialEntries: [originalUrl]
    });
    const store = createStore(
      history,
      {
        router: {
          location: {
            pathname: originalUrl
          }
        }
      },
      req.cookies
    );
    routes.some(route => {
      const match = matchPath(originalUrl, route);
      if (match) {
        getFetcher(route)(store.dispatch, store.getState)
          .then(() => {
            res.status(200).send(renderHtml(history, store));
          })
          .catch(error => {
            next(error);
          });
      }
      return match;
    });
  });
};
