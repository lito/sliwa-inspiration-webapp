// @flow

import compression from 'compression';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import express from 'express';
// eslint-disable-next-line
import { proxyHandler as sharedProxyHandler } from 'visar-shared';
import appHandler from './app';
import userProxyHandler from './userPoxy';
import { STATIC_PATH, WEB_PORT } from '../shared/config';
import { isProd } from '../shared/utils/env';

const app = express();

app.use(compression());
app.use(STATIC_PATH, express.static('dist'));
app.use(STATIC_PATH, express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
sharedProxyHandler(app);
userProxyHandler(app);
appHandler(app);

app.listen(WEB_PORT, () => {
  // eslint-disable-next-line no-console
  console.log(
    `Server running on port ${WEB_PORT} ${
      isProd
        ? '(production)'
        : '(development).\nKeep "yarn dev:wds" running in an other terminal'
    }`
  );
});
