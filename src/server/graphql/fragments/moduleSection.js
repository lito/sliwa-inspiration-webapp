exports.default = `
entity {
  ... on NodeModuleSection {
    title
    type: entityBundle
    name: fieldName
    headline: fieldHeadline
    hasWindowHeight: fieldHasWindowHeight
    image: fieldImage {
      url
    }
    components: fieldComponents {
      entity {
        type: entityBundle
        ... on NodeModuleTeaserList {
          title
          backgroundColor: fieldBackgroundColor
          items: fieldItems {
            entity {
              ... on NodeModuleTeaser {
                title
                headline: fieldHeadline
                image: fieldImage {
                  url
                }
                link: fieldLink {
                  entity {
                    ... on NodeModuleLink {
                      headline: fieldHeadline
                      target: fieldTarget
                    }
                  }
                }
              }
            }
          }
        }
        ... on NodeModuleGallery {
          images: fieldImages {
            url
          }
        }
        ... on NodeModuleMedia {
          title
          files: fieldMediaFiles {
            entity {
              type: entityBundle
              ... on NodeModuleImage {
                image: fieldImage {
                  url
                }
              }
              ... on NodeModuleVideo {
                poster: fieldImage {
                  url
                }
                video: fieldVideo {
                  entity {
                    url
                    filemime
                  }
                }
              }
            }
          }
        }
        ... on NodeModuleText {
          title
          body {
            value
          }
        }
        ... on NodeModuleForm {
          title
          elements: fieldFormElements {
            entity {
              entityBundle
              ... on NodeModuleFormInput {
                type: fieldType
                value: fieldValue
                name: fieldName
                placeholder: fieldPlaceholder
              }
              ... on NodeModuleFormTextarea {
                name: fieldName
                placeholder: fieldPlaceholder
                rows: fieldRows
              }
              ... on NodeModuleFormRecaptcha {
                sitekey: fieldSitekey
                size: fieldSize
                render: fieldRender
                hl: fieldHl
              }
              ... on NodeModuleFormButton {
                type: fieldType
                text: fieldButtonText
              }
            }
          }
        }
      }
    }
  }
}
`;
