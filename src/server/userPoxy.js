// @flow

import axios from 'axios';
import {
  MESSAGE_BASE_URL,
  MESSAGE_CONSUMER,
  MESSAGE_RECIPIENT
} from '../shared/config';

const errorHandler = error => {
  let { message } = error;
  if (error.response) {
    message = error.response.data.message;
  }
  return message;
};

export default (app: Object) => {
  app.post('/api/message', (req, res, next) => {
    const payload = Object.assign({}, req.body, {
      consumer: MESSAGE_CONSUMER,
      recipient: MESSAGE_RECIPIENT
    });
    axios
      .post(`${MESSAGE_BASE_URL}/message`, payload)
      .then(apiRes => {
        res.status(200).send(apiRes.data);
      })
      .catch(err => {
        const error = errorHandler(err);
        res.status(500).send(error);
        next(error);
      });
  });
};
