// @flow

import path from 'path';
import webpack from 'webpack';
// eslint-disable-next-line
import { getConfig } from 'visar-shared';
import { WDS_PORT } from './src/shared/config';
import { isProd } from './src/shared/utils/env';

module.exports = {
  devtool: 'cheap-eval-source-map',
  entry: ['react-hot-loader/patch', './src/client'],
  output: {
    filename: 'js/bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: isProd ? '/static/' : `http://localhost:${WDS_PORT}/dist/`
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader',
        exclude: /node_modules\/(?!(visar-shared|fs)\/).*/
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  },
  devServer: {
    port: WDS_PORT,
    hot: true,
    contentBase: './dist',
    headers: { 'Access-Control-Allow-Origin': '*' }
  },
  plugins: [
    new webpack.DefinePlugin({
      __VSR_CONFIG__: JSON.stringify(getConfig()),
      __BROWSER__: '"true"'
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  ]
};
