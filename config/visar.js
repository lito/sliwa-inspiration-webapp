// eslint-disable-next-line
var moduleSection = require('../src/server/graphql/fragments/moduleSection')
  .default;

exports.default = {
  STATIC_PATH: '/public/images',
  API_BASE_URL: 'http://content.sliwa-inspiration.com',
  GRAPHQL: {
    queries: {
      page_text: `
      page: nodeQuery(filter: {type: "page_text", title: $title}) {
        entities {
          ... on NodePageText {
            title
            headline: fieldHeadline
            body {
              value
            }
          }
        }
      }`,
      page_dynamic: `
      page: nodeQuery(filter: {type: $type, title: $title}) {
        entities {
          ... on NodePageDynamic {
            entityBundle
            title
            mainSections: fieldSections {
              ${moduleSection}
            }
            sidebarSections: fieldSidebar {
              ${moduleSection}
            }
          }
        }
      }
      `
    }
  }
};
