# Visarsoft stack

## Install dependencies
```sh
yarn install
```

## Install flow definitions for styled-components
```sh
yarn flow-typed install styled-components@<version>
```

## Start dev server
```sh
yarn start
```
```sh
yarn dev:wds
```
